import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import store from './app/store.jsx';
import { Provider } from 'react-redux';
import Home from './routes/Home.jsx';
import Root from './routes/root.jsx';
import ErrorPage from './error-page.jsx';
import Login from './routes/Login.jsx';
import Signup from './routes/Signup.jsx';
import Cart from './routes/Cart.jsx';
import Learn from './routes/Learn.jsx';
import WhoWeAre from './routes/WhoWeAre.jsx';
import ShopAllProduct from './routes/ShopAllProduct.jsx';



const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>,
    errorElement: <ErrorPage/>,
    children: [
      {
        path:'/',
        element:<Home/>
      },
      {
        path: '/login',
        element: <Login/>
      },
      {
        path: '/signup',
        element: <Signup/>
      },
      {
        path: '/cart',
        element: <Cart/>
      },
      {
        path: '/learn',
        element: <Learn/>
      },
      {
        path: '/whoweare',
        element: <WhoWeAre/>
      },
      {
        path: '/shopallproduct',
        element: <ShopAllProduct/>
      }
      
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
     <RouterProvider router={router} />
  </Provider>,
)
