import React from "react";
import { Link } from "react-router-dom";

function CTALink(props){
    return(
        <Link to={props.to}  className="bg-green-800 hover:bg-green-600 px-9 py-3 rounded-md text-white mt-4">{props.text} </Link>
    )
}

export default CTALink