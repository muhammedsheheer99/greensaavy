import React, { useState} from "react";
import { useSelector } from "react-redux";
import { Outlet, Link } from "react-router-dom";

function Header(props){
    
  const [showDropdown, setShowDropdown] = useState(false);

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const [showMobileMenu, setShowMobileMenu] = useState(false);


    const toggleMobileMenu = () => {
    setShowMobileMenu(!showMobileMenu);

    if(!showMobileMenu){
      document.body.classList.add('overflow-hidden')
    }
    else{
      document.body.classList.remove('overflow-hidden')
    }
    };

    const cartItems = useSelector(state => state.cart.cartItems)
    console.log(cartItems)

    return(
    <header className="w-full">
    <div className='h-24  px-6 xl:px-24 bg-gray-100 flex flex-row justify-between items-center shadow-lg '>
    <Link className="block lg:hidden" to={'#'} onClick={toggleMobileMenu}>
     <img className="w-7 h-7" src="/icons/menu.svg" alt="menu bar" />
    </Link>
    <Link to={'/'}>
      <img className='w-40 lg:w-60' src="/images/greensaavy.png" alt="pagelogo" />
    </Link>
    <nav className="hidden lg:block">
    <ul className='flex flex-row gap-10 font-medium text-gray-700 text-xl'>
      <li>
       <Link className="hover:underline"  to={'/'}>Home</Link>
      </li>
      <li>
         <Link className="hover:underline"  to='#' onClick={toggleDropdown}>Shop</Link>
                 {showDropdown && (
                   <ul className="absolute mt-2 py-2 bg-white border shadow-md">
                     <li><Link className="block px-4 py-2 hover:underline" to="#">Shop All Product</Link></li>
                     <li><Link className="block px-4 py-2 hover:underline" to="#">Shop by Brand</Link></li>
                     {/* Add more categories as needed */}
                   </ul>
                 )}
         </li>
      <li>
        <Link className="hover:underline" to='#'>Rewards</Link>
      </li>
      <li>
        <Link className="hover:underline" to={'/learn'}>Learn</Link>
      </li>
      <li>
        <Link className="hover:underline" to={'/whoweare'}>Who We Are</Link>
      </li>
    </ul>
    </nav>
    {showMobileMenu && (
       <nav className="flex flex-col justify-between lg:hidden absolute top-0 left-0 w-4/5 h-full bg-white py-4">
       <ul className="flex flex-col px-4">
        <li className="block py-2">
           <button onClick={toggleMobileMenu}>
              <img
                className="w-6 h-6"
                src="/icons/close.svg"
                alt="close menu"
              />
            </button>
        </li>
         <li>
          <Link onClick={toggleMobileMenu} className="block py-2 hover:underline text-lg font-semibold"   to={'/'}>Home</Link>
         </li>
         <hr />
         <li>
         <Link className="block py-2 hover:underline text-lg font-semibold"  to='#' onClick={toggleDropdown}>Shop</Link>
                 {showDropdown && (
                   <ul className="absolute left-0 mt-2 bg-white  w-full h-96">
                     <li><Link className="block px-4 py-2 hover:underline" to="#">Shop All Product</Link></li>
                     <li><Link className="block px-4 py-2 hover:underline" to="#">Shop by Brand</Link></li>
                     
                     {/* Add more categories as needed */}
                   </ul>
                 )}
         </li>
         <hr />
         <li>
           <Link onClick={toggleMobileMenu} className="block py-2 hover:underline text-lg font-semibold"  to='#'>Rewards</Link>
         </li>
         <hr />
         <li>
           <Link onClick={toggleMobileMenu} className="block py-2 hover:underline text-lg font-semibold"  to={'/learn'}>Learn</Link>
         </li>
         <hr />
         <li>
           <Link onClick={toggleMobileMenu} className="block py-2 hover:underline text-lg font-semibold"  to={'/whoweare'}>Who We Are</Link>
         </li>
       </ul>
       <Link onClick={toggleMobileMenu} className="block py-2 "  to="/login">
          <div className="flex flex-row items-center gap-2 px-2">
          <img className=" w-9 h-9" src="icons/account1.svg" alt="" />
          <span className="text-lg font-semibold">Login</span>
          </div>
        </Link>
       </nav>
    )} 
    <div className="flex flex-row gap-2">
        <Link to="/login">
          <img className="hidden lg:block w-9 h-9" src="icons/account.svg" alt="" />
        </Link>
        <Link className="flex flex-row gap-1" to={'/cart'}>
          <img className="w-7 lg:w-9 h-7 lg:h-9" src="icons/cart.svg" alt="cart icon"></img>
          <span className="text-red-800 font-semibold">{cartItems.length}</span>
        </Link>
    </div>
    </div>
    </header>
    )
}

export default Header