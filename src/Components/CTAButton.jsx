import React from "react";

function CTAButton(props){
    return(
        <button type={props.type?props.type:''} action={props.action} className="bg-green-800 px-9 py-3 rounded-md text-white mt-4">{props.text}</button>
    )
}

export default CTAButton