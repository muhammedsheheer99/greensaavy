import React from "react";
import { Outlet, Link } from "react-router-dom";
import CTAButton from "./CTAButton";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cart/cartSlice";


function Products(props){
    console.log(props.product)
    const product = (props.product)

    const dispatch = useDispatch()
    return (
        <li className="flex flex-col justify-between gap-8">
            <div className="flex flex-col gap-3">
              <img className="w-[10rem] md:w-[20rem] lg:w-[22rem] h-[8rem] md:h-[16rem] lg:h-[16rem] rounded-md	" src={product.image} alt="product images" />
              <Link to={'#'}>
              <h3 className="text-sm lg:text-xl ">{product.title}</h3>
              </Link>
              <Link to={'#'}>
              <span className="text-base lg:text-2xl font-semibold">{product.description}</span>
              </Link>
              <span className="text-base lg:text-xl">${product.price}</span>
            </div>
            <div>
            <button onClick={()=>{dispatch(addToCart({product: product, quantity: 1}))}} className="w-full bg-green-800 hover:bg-green-600 py-2 text-white">Add to cart</button>
            </div>
            
            
        </li>
    )
}

export default Products