import React from "react";
import { Outlet, Link } from "react-router-dom";

function Footer(props){
    return(
        <footer className="mt-10 w-full">
            <img className="w-full h-full" src="images/s5.jpg" alt="" />
            <div className=" px-2 xl:px-24 lg:h-80 bg-green-900">
            <div className="flex flex-col lg:flex-row justify-between items-center ">
                <div className="h-80 flex flex-col justify-center gap-4">
                    <div>
                        <img className="h-16 mb-" src="/images/gsfooter.png" alt="site footer logo" />
                    </div>
                    <div className="flex flex-row px-4 gap-10">
                        <img src="/icons/certified.webp" alt="" />
                        <img src="/icons/planet.webp" alt="" />
                        <img src="/icons/climate.webp" alt="" />
                    </div>
                    <div>
                        <ul className="flex flex-row items-center px-4 ">
                            <li className="border hover:bg-white px-2 py-1">
                                <Link to='#'><img src="/icons/facebook.svg" alt="" /></Link>
                            </li>
                            <li  className="border hover:bg-white px-2 py-1">
                                <Link to='#'><img src="/icons/instagram.svg" alt="" /></Link>
                            </li>
                            <li  className="border hover:bg-white px-2 py-1">
                                <Link to='#'><img src="/icons/pinterest.svg" alt="" /></Link>
                            </li>
                            <li  className="border hover:bg-white px-2 py-1 ">
                                <Link to='#'><img src="/icons/tiktok.svg" alt="" /></Link>
                            </li>
                        </ul>
                    </div>  
                </div>
                <div className="hidden lg:block lg:flex flex-col gap-6 ">
                    <h3 className="text-white font-sans">EXPLORE</h3>
                    <ul className="flex flex-col gap-2 text-white">
                        <li>
                            <Link className="hover:text-gray-400" to='#'>Shop All Brands</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to='#'>Sustainable Corporate Gifting</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to='#'> Bulk & Custom Orders</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to='#'> Clarance</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to='#'>Gift Cards</Link>
                        </li>
                        
                    </ul>
                </div>
                <div className="hidden lg:block lg:flex flex-col gap-6 ">
                    <h3 className="text-white font-sans">ABOUT US</h3>
                    <ul  className="flex flex-col gap-2 text-white">
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Impact</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Our Story</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Sustainable</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Sourcing</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Blog</Link>
                        </li>
                    </ul>
                </div>
                <div className="hidden lg:block lg:flex flex-col gap-6 ">
                    <h3 className="text-white font-sans">GET INVOLVED</h3>
                    <ul  className="flex flex-col gap-2 text-white">
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Job</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Opportunities</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Collective</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>Affiliates</Link>
                        </li>
                        <li>
                            <Link className="hover:text-gray-400" to={'#'}>In the Press</Link>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
            
        </footer>
    )
}

export default Footer