import React, { useEffect, useState } from "react";
import { Outlet, Link } from "react-router-dom";
import CTALink from '../Components/CTALink'
import Products from "../Components/Products";
import axios from "axios";


function Home() {

  const [products, setProducts] = useState([])

  useEffect(()=>{
    axios.get('https://greensaavyback.onrender.com/products')
    .then((data)=>{
      console.log(data.data)
      setProducts(data.data)
    })
    .catch((error)=>{
      console.log(error)
    })
  }, [])
 
  return (
    <main className="w-full">
      <section class="flex flex-col justify-center items-center gap-12 w-full h-[33rem] md:h-[48rem]  bg-contain md:bg-cover bg-center bg-[url('/images/s.jpg')]">
          <div className='flex flex-col justify-center items-center gap-8 top-48 left-56'>
            <h2 className='px-2 text-lg md:text-2xl text-green-900 font-semibold font-sans'>Discover over 200 environmentally conscious brands</h2>
            <p className='px-2 text-3xl md:text-6xl text-green-900 font-semibold font-sans'> Effortless sustainable shopping  <br/>empowering conscious choices</p>
          </div>
          <div className='flex flex-row gap-4'>
            <div className="p-4 bg-orange-600 rounded hover:bg-orange-400 font-semibold text-white">
            <a href={'/shopallproduct'}>
              Shop All Product
            </a>
            </div>
            <div className="p-4 bg-orange-600 rounded hover:bg-orange-400 font-semibold text-white">
            <a href='#'>
              Shop by Brand
            </a>
            </div>
          </div>
      </section>
      <section className='container mx-auto'>
       <div className='flex flex-col items-center gap-4 mt-20'>
       <h2 className='text-orange-500 font-sans'>Embark on your sustainable journey </h2>
       <h3 className='text-4xl md:text-5xl text-amber-950 font-sans'>Join the movement</h3>
       <ul className="grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8 mt-8">
        {
          products.map((product,index) =>
            <Products key={index} product={product}/>
            )
        }
       </ul>
       </div>
      </section>
      <section className="container mx-auto mt-16">
       <h3 className="flex flex-col items-center font-bold font-sans text-xl md:text-3xl lg:text-5xl text-slate-800 mb-10">
       <span className="text-center">Take the next step toward</span>
       <span className="text-center">a sustainable lifestyle</span>
       </h3>
        <div className="flex flex-col lg:flex-row justify-center items-center gap-6">
        <div class="flex flex-col justify-center gap-4 ease-in-out delay-150 hover:-translate-x-4 hover:scale-95 w-[20rem] md:w-[34rem] lg:w-[40rem] h-[18rem] md:h-[34] lg:h-[48rem] bg-cover  bg-center rounded-md	 bg-[url('/images/backpack.jpg')]">
          <Link className="text-xl md:text-3xl text-white text-center font-semibold font-sans" to={'#'} >Cotopaxi: etical backpacks and apparel</Link>
          <Link className="text-xs text-white text-center underline font-semibold font-sans" to={'#'}>MAKE YOUR NEXT ADVENTURE COLORFUL</Link>
        </div>
        <div className="flex flex-col justify-center gap-8">
        <div class="flex flex-col justify-center gap-4 ease-in-out delay-150 hover:-translate-x-4 hover:scale-95 w-[20rem] md:w-[34rem] lg:w-[40rem] h-[18rem] lg:h-[23rem]  bg-cover  bg-center rounded-md  bg-[url('/images/newproduct.jpg')]">
          <Link className="text-xl md:text-3xl text-white text-center font-semibold font-sans" to={'#'}>Over 100 new products just landed</Link>
          <Link className="text-xs text-white text-center underline font-semibold font-sans" to={'#'}>SHOP NEW ARRIVALS</Link>
        </div>
        <div class="flex flex-col justify-center  gap-4 ease-in-out delay-150 hover:-translate-x-4 hover:scale-95 w-[20rem] md:w-[34rem] lg:w-[40rem] h-[18rem] lg:h-[23rem] bg-cover  bg-center rounded-md	 bg-[url('/images/bars.jpg')]">
          <Link className="text-xl md:text-3xl text-white text-center font-semibold font-sans" to={'#'}>Upgrade with premium shampoos and bars</Link>
          <Link className="text-xs text-white text-center underline font-semibold font-sans" to={'#'}>SHOP HAIR CARE</Link>
        </div>
        </div>
        </div>
      </section>
      <section className="container mx-auto px-4 mt-16">
        <div className="flex flex-col lg:flex-row justify-center items-center gap-8">
          <div className="flex flex-col gap-8">
            <span className="text-lg text-orange-600 font-semibold">Our Shared Impact</span>
            <h3 className="text-3xl lg:text-6xl font-bold">Committing to <br /> the Planet</h3>
            <p className="text-xs md:text-lg">
              Shopping with GreenSavvy signals your commitment to a more <br /> sustainable world. Our array of certifications showcases our dedication <br /> to a healthier planet, illustrating the tangible impact your choices have on <br /> Earth. Dive into these certifications to explore how we're advancing towards <br /> a greener future.
            </p>
            <div>
            <CTALink to={'/learn'} text='Learn More'/>
            </div>
          </div>
          <div>
            <img className="w-[20rem] lg:w-[40rem] h-[10rem] lg:h-[20rem]" src="/images/ourplanet.webp" alt="planet certification" />
          </div>
        </div>
      </section>
     
    </main>
  );
}

export default Home
