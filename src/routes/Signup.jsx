import React, { useRef, useState } from "react";
import { object, string} from 'yup';
import { Outlet, Link } from "react-router-dom";
import CTAButton from "../Components/CTAButton";
import axios from "axios";



function Signup(props){

    const signupForm = useRef(null)

    const signupSchema = object({
        name: string().required(),
        email: string().email().required(),
        password: string().required()
    })

    const [error, setError] = useState('')

    async function handleSubmit(e){
        e.preventDefault()
        
        const form = signupForm.current
        const name = form['name'].value
        const email = form['email'].value
        const password = form ['password'].value

        try{
            const userDetails = await signupSchema.validate({name,email,password})
            const payload = {
                name: name,
                email: email,
                password: password
            }

            axios.post('https://greensaavyback.onrender.com/users/signup', payload)
            .then(data => {console.log(data)})
            .catch(err => {console.log(err)})
        }
        catch(error){
            setError(error.errors)
        }
    }

    return(
        <main className="container mx-auto px-4 py-16">
            <section className="flex flex-col justify-center items-center ">
                <div className="flex flex-col items-center pb-16 gap-10">
                <span className="text-6xl font-medium">Register</span>
                <span className="text-lg text-gray-600">Please fill in the fields below:</span>
                </div>
                <form ref={signupForm} className="w-full flex flex-col items-center gap-3" onSubmit={handleSubmit}>
                    <label htmlFor="name"></label>
                    <input type="text" id="name" placeholder="Name" className="border border-gray-400 rounded-md  w-80 h-14 px-4  text-xl" />
                    <label htmlFor="email"></label>
                    <input type="email" id="email" placeholder="Email" className="border border-gray-400 rounded-md  w-80 h-14 px-4  text-xl"/>
                    <label htmlFor="password"></label>
                    <input type="password" id="password" placeholder="Password" className="border border-gray-400 rounded-md w-80 h-14 px-4 text-xl " />
                    <CTAButton type='submit' action={()=>{}} text='Signup' />
                </form>
                <span className="mt-4 text-sm text-red-600">{error}</span>
                <div className="flex flex-row gap-2 text-gray-600 mt-4"> 
                    <span>Already have an account?</span>
                    <Link className="underline" to="/login">Login</Link>
                </div>
                
            </section>
        </main>
    )
}

export default Signup