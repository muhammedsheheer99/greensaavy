import React, { useRef, useState } from "react";
import { object, string} from 'yup';
import { Outlet, Link } from "react-router-dom";
import CTAButton from "../Components/CTAButton";
import axios from "axios";



function Login(props){

    const loginForm = useRef(null)

    const loginSchema = object({
        email: string().email().required(),
        password: string().required()
    })

    const [error, setError] = useState('')
    const [notFoundError, setNotFoundError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)

    async function Login(userDetails){
        try{
        const loginDetails= await axios.post('https://greensaavyback.onrender.com/users/login', userDetails)
        console.log(loginDetails)
        }
        catch(error){
            switch(error.response.status){
                case 404: setNotFoundError(true)
                break;
                case 401: setPasswordError(true)
            }

        }
        
    }

    async function handleSubmit(e){
        e.preventDefault()
        
        const form = loginForm.current
        const email = form['email'].value
        const password = form ['password'].value

        try{
            const userDetails = await loginSchema.validate({email,password})
            Login(userDetails)
        }
        catch(error){
            setError(error.errors)
        }
    }

    return(
        <main className="container mx-auto px-4 py-16">
            <section className="flex flex-col justify-center items-center ">
                <div className="flex flex-col justify-center items-center pb-16 gap-10">
                <span className="text-6xl font-medium">Login</span>
                <span className="text-lg text-gray-600">Please enter your e-mail and password:</span>
                </div>
                <form ref={loginForm} className="flex flex-col items-center gap-3" onSubmit={handleSubmit}>
                    <label htmlFor="email"></label>
                    <input type="email" id="email" placeholder="Email" className="border border-gray-400 rounded-md  w-80 h-14 px-4  text-xl"/>
                    {notFoundError?<span className="text-xs text-red-500">Email is not found </span>:<></>}
                    <label htmlFor="password"></label>
                    <input type="password" id="password" placeholder="Password" className="border border-gray-400 rounded-md  w-80 h-14 px-4  text-xl " />
                    {passwordError?<span className="text-xs text-red-500">password incorrect </span>:<></>}
                    <CTAButton type='submit' action={()=>{}} text='Login' />
                </form>
                <span className="mt-4 text-sm text-red-600">{error}</span>
                <div className="flex flex-col items-center mt-10 text-gray-600 text-lg">
                    <p className="text-center">First time logging in on our new site? Contact our team on chat if you have trouble logging in.</p>
                    
                </div>
                <div className="flex flex-row gap-2 text-gray-600 mt-4"> 
                    <span>New Customer?</span>
                    <Link className="underline" to={'/signup'}>Create an account</Link>
                </div>
                
            </section>
        </main>
    )
}

export default Login