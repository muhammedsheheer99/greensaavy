import React from "react";

function Cart (props){
    return(
        <section>
            <div className="flex flex-col gap-6 mt-14">
            <span className="text-center text-4xl lg:text-5xl font-semibold">Cart</span>
            <p className="text-center text-base lg:text-lg">Spend $50 get free shipping!</p>
            </div>
        </section>
        
    )
}

export default Cart