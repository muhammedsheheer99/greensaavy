import React, { useEffect, useState } from "react";
import { Outlet, Link } from "react-router-dom";
import Products from "../Components/Products";
import axios from "axios";

function ShopAllProduct (props) {

    const [products, setProducts] = useState([])

  useEffect(()=>{
    axios.get('http://localhost:3000/products')
    .then((data)=>{
      console.log(data.data)
      setProducts(data.data)
    })
    .catch((error)=>{
      console.log(error)
    })
  }, [])

    return(
        <section className="lg:container lg:mx-auto mx-4 mt-10">
         <span className="text3xl lg:text-5xl font-semibold ">All Products</span>
         <ul className="grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8 mt-8 lg:mt-14">
          {
           products.map((product,index) =>
            <Products key={index} product={product}/>
            )
          }
         </ul>
        </section>
    )
} export default ShopAllProduct