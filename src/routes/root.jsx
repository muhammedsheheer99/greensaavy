import React from "react";
import Header from "../Components/Header";
import { Outlet } from "react-router-dom";
import Footer from "../Components/Footer";

function Root(props){
    return(
        <>
        <Header/>
        <Outlet/>
        <Footer/>
        </>
        
    )
}

export default Root