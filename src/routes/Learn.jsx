import React, {useEffect} from "react";

function Learn (props){
    useEffect(() => {
        window.scrollTo(0, 0); // Scrolls the window to the top when the component mounts
      }, []); 
    return(
        <section>
         <div className="flex flex-col justify-center gap-4 w-full h-[20rem] md:h-[28rem]  bg-contain md:bg-cover bg-center bg-[url('/images/learn4.jpg')]">
          <span className="text-center text-white font-bold text-sm ">FEATURED ARTICLE</span>
          <span className="text-center text-white font-semibold text-4xl lg:text-5xl">Your Sustainability Journey</span>
          <span className="text-center text-white font-semibold text-sm  underline">READ ARTICLE</span>
         </div>
       <div className="lg:container lg:mx-auto mx-4 mt-12 text-xs lg:text-base">
        <p>
        Welcome to GreenSaavy, where sustainability isn't just a concept—it's a philosophy that guides our every step. Our platform is a haven for those seeking not just eco-friendly products, but a comprehensive lifestyle rooted in conscious consumption. At GreenSaavy, we're on a mission to redefine how we interact with the planet. We offer a diverse range of thoughtfully curated, sustainable products that seamlessly integrate into your daily routine while minimizing environmental impact. From reusable essentials to innovative zero-waste solutions, each item embodies our dedication to fostering a greener world.        </p>
        <br />
        <p>
        Our ethos is grounded in education and empowerment. Beyond offering products, GreenSaavy is a knowledge hub, providing an extensive repository of articles, guides, and resources aimed at guiding you on your sustainable living journey. Dive into our content-rich platform, where you'll find practical tips, expert insights, and actionable steps to help you navigate the path toward a more eco-conscious lifestyle. Whether you're curious about composting, looking to adopt a plastic-free kitchen, or seeking ethical fashion choices, our resources are designed to support and inspire your choices.        </p>
        <br />
        <p>
        GreenSaavy isn't just about transactions; it's about building a community united by a shared passion for sustainability. Our platform serves as a meeting ground for like-minded individuals to exchange ideas, experiences, and tips. We believe that collective action is the key to driving meaningful change. Join us in discussions, share your journey, and be part of a movement that champions mindful consumption and environmental stewardship.        </p>
        <br />
        <p>
        We meticulously curate our collection to ensure that every product reflects our commitment to quality, sustainability, and ethical production. Each item undergoes stringent scrutiny, ensuring it meets our criteria for durability, functionality, and eco-friendliness. Our dedication to sourcing from responsible suppliers and manufacturers is unwavering, allowing us to offer you products that not only align with your values but also stand the test of time.        </p>
        <br />
        <p>
        Beyond selling products, GreenSaavy aims to be a beacon of inspiration. Our blog delves deep into various aspects of sustainability, offering a treasure trove of information and insights. Dive into topics like renewable energy, reducing single-use plastics, sustainable travel, and more. We're committed to providing valuable, well-researched content that empowers you to make informed decisions and effect positive change in your life and beyond.        </p>
        <p>
        We firmly believe that change starts with individual choices. Every product you select from GreenSaavy is a vote for a sustainable future. We're here to make conscious living accessible and enjoyable. Explore our wide array of eco-friendly alternatives, from organic personal care products to eco-conscious home goods and earth-friendly gadgets. Each purchase contributes to a larger movement—one that embraces sustainability as a way of life.
        </p>
        <br />
        <p>
        GreenSaavy operates with a commitment to environmental responsibility. We strive for carbon neutrality in our operations and packaging, endeavoring to minimize our ecological footprint. Our dedication extends to continuously seeking ways to improve our practices, collaborating with partners who share our values, and adopting innovative approaches that advance sustainability in all aspects of our business.
        </p>
        <br />
        <p>
        Transparency and accountability are at the core of GreenSaavy's values. We invite you to engage with us, ask questions, and participate in our journey towards a more sustainable future. Your feedback drives us to evolve, innovate, and refine our offerings and practices. Together, let's create a dialogue that fosters meaningful change and empowers individuals to make choices that benefit both them and the environment.
        </p>
        <br />
        <p>
        Thank you for choosing GreenSaavy as your companion on this transformative journey. Your support fuels our commitment to championing sustainability. Together, let's continue to pave the way for a world where conscious choices resonate positively, leaving a lasting impact for generations to come. Join us in creating a future where sustainability isn't just an option—it's a way of life that enriches and preserves our beautiful planet Earth.
        </p>
       </div>
        </section>
       
    )
}

export default Learn