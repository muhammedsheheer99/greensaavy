import React from "react";

function WhoWeAre (props) {
    return(
        <section>
            <div className="flex flex-col justify-center gap-4 w-full h-[20rem] md:h-[28rem]  bg-contain md:bg-cover bg-center bg-[url('/images/ourstory1.jpg')]">
                <span className="text-center text-white text-3xl lg:text-5xl font-semibold">Our Story</span>
            </div>
            <div className="mt-14 mx-3 lg:container lg:mx-auto  text-sm lg:text-base ">
                <p>
                   At GreenSaavy, our journey began with a simple yet profound belief—that each choice we make holds the power to shape our planet's future. Rooted in a passion for sustainability, GreenSaavy emerged as a testament to our unwavering commitment to a greener, more eco-conscious world.
                </p>
                <br />
                <p>
                Driven by a collective vision, our team, diverse in backgrounds but united by a shared purpose, embarked on a mission to redefine the way we interact with our environment. From the outset, we envisioned a platform where sustainability isn't a buzzword but an ingrained lifestyle.
                </p>
                <p>
                Founded on principles of ethical consumption and environmental responsibility, GreenSaavy isn't merely an e-commerce destination. It's a movement—a convergence of innovation, education, and advocacy. Every facet of our platform, meticulously crafted, resonates with our dedication to fostering conscious consumerism.
                </p>
                <br />
                <p>
                In our pursuit to make sustainability accessible, we curated a collection of over 200 environmentally conscious brands. Each product, meticulously chosen, mirrors our commitment to quality, functionality, and above all, minimizing our ecological footprint.
                </p>
                <br />
                <p>
                But our story isn't just about products; it's about the communities we build and the dialogues we foster. Through our platform, we've cultivated a vibrant ecosystem—a space where like-minded individuals converge, sharing insights, experiences, and inspiration for a sustainable future.
                </p>
                <br />
                <p>
                GreenSaavy is more than a destination; it's an educational hub. Through insightful articles, actionable guides, and thought-provoking content, we empower our community to embrace sustainability effortlessly. We believe education is the catalyst for change.
                </p>
                <br />
                <p>
                Guided by a holistic approach, we continuously challenge norms and evolve. Our commitment isn't static; it's dynamic and responsive to the ever-evolving landscape of sustainable practices. We listen, adapt, and innovate.
                </p>
                <br />
                <p>
                Committed to transparency, our journey isn't without challenges. Yet, with each obstacle, we grow, learn, and redefine our commitments. We stand accountable for our actions, transparent in our operations, and steadfast in our dedication to sustainability.
                </p>
                <br />
                <p>
                GreenSaavy isn't just a platform; it's a narrative—a narrative that amplifies the voices of individuals, communities, and the planet. Together, we're authoring a story of hope, resilience, and a future where every choice echoes our pledge to preserve our planet for generations to come.
                </p>
            </div>
        </section>
    )
}

export default WhoWeAre